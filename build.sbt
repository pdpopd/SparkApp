import sbt._

ThisBuild / name := "SparkApp"
ThisBuild / organization := "ivan"
ThisBuild / version := "0.2"
ThisBuild / scalaVersion  := "2.11.11"

val sparkV = "2.3.0"

val app     = project("app")
  .settings(
    mainClass := Some("ivan.app.Main"),
    version := "1.0-SNAPSHOT"
  )

def project(id: String) = Project(id, base = file(id))
  .enablePlugins(JavaAppPackaging, DockerPlugin, UniversalPlugin)
   .settings(
    libraryDependencies ++= {
      Seq(
        "com.typesafe" % "config" % "1.3.3",
        "org.apache.spark" %% "spark-core" % sparkV,
        "org.apache.spark" %% "spark-sql" % sparkV,
        "org.apache.spark" %% "spark-mllib" % sparkV,
        "org.specs2" %% "specs2-core" % "4.0.3-83f81a8-20180227163433" % Test
      )
    }
  )

