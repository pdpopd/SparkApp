package ivan.app.pipes

case class Pipe3ConfRow(existing_col_name: String, new_col_name: String, new_data_type: String, date_expression: Option[String])
