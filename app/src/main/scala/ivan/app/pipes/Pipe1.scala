package ivan.app.pipes

import com.typesafe.config.Config
import org.apache.spark.sql.{DataFrame, SparkSession}
import ivan.app.Utils._

/**
  * CSV reading
  */
class Pipe1(conf: Config) extends Pipe(conf) {
  val file = conf.getString("file")

  def run(df: DataFrame, spark: SparkSession):DataFrame = {
    val ro = conf.getConfig("read_options").asMap
    val r = spark.read
    ro.map{case (k, v) => r.option(k,v)}
    r.csv(file)
  }
}
