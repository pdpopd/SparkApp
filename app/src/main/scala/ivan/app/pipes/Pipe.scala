package ivan.app.pipes

import com.typesafe.config.Config
import org.apache.spark.sql.{DataFrame, SparkSession}

abstract class Pipe(conf: Config) {
  def run(df: DataFrame, spark: SparkSession):DataFrame
}
