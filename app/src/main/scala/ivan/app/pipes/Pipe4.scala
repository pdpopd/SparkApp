package ivan.app.pipes

import java.io.File

import com.typesafe.config.Config
import org.apache.commons.io.FileUtils
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.json4s.jackson.JsonMethods.pretty
import org.json4s.{JArray, JField, JInt, JObject, JString}

/**
  * Word counting and saving result to file
  */
class Pipe4(conf: Config)  extends Pipe(conf) {

  val fileout = new File(conf.getString("file"))

  def run(df: DataFrame, spark: SparkSession):DataFrame = {

    val out = df.columns.flatMap { cn =>
      val c = col(cn)
      df.select(c)
        .filter(c.isNotNull).rdd
        .map(r => (r.get(0).toString, 1L))//.rdd
        .reduceByKey(_ + _).collect()
        .map(r => (cn, r._1, r._2))
    }.groupBy(_._1).map{ r=>
      val arr = r._2.map(t => JObject(JField(t._2, JInt(t._3)))).toList
      JObject(JField("Column",JString(r._1)), JField("Unique_values",JInt(arr.length)), JField("Values",JArray(arr)))
    }.toList

    FileUtils.writeStringToFile(fileout, pretty(JArray(out)))
    df
  }
}
