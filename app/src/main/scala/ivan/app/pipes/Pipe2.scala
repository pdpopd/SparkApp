package ivan.app.pipes

import com.typesafe.config.Config
import org.apache.spark.sql.{DataFrame, SparkSession}

/**
  * Filter
  */
class Pipe2(conf: Config) extends Pipe(conf) {
  def run(df: DataFrame, spark: SparkSession):DataFrame = {
    df.filter { r =>
      r.toSeq.forall {
        case null => true
        case s: String => s.trim.nonEmpty
      }
    }
  }
}
