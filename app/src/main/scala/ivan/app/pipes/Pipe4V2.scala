package ivan.app.pipes

import java.io.File

import com.typesafe.config.Config
import org.apache.commons.io.FileUtils
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.json4s.jackson.JsonMethods.pretty
import org.json4s.{JArray, JField, JInt, JObject, JString}

/**
  * Word counting and saving result to file
  */
class Pipe4V2(conf: Config)  extends Pipe(conf) {

  val fileout = new File(conf.getString("file"))

  def run(df: DataFrame, spark: SparkSession):DataFrame = {

    df.createOrReplaceTempView("pipe4")

    val out = df.columns.flatMap { cn =>
      spark.sql(s"select $cn, count($cn) from pipe4 group by $cn having $cn is not null").collect()
        .map(r => (cn, r.get(0).toString, r.getLong(1)))
    }.groupBy(_._1).map{ r=>
      val arr = r._2.map(t => JObject(JField(t._2, JInt(t._3)))).toList
      JObject(JField("Column",JString(r._1)), JField("Unique_values",JInt(arr.length)), JField("Values",JArray(arr)))
    }.toList

    FileUtils.writeStringToFile(fileout, pretty(JArray(out)))
    df
  }
}
