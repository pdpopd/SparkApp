package ivan.app.pipes

import java.io.File
import java.text.SimpleDateFormat

import com.typesafe.config.Config
import org.apache.spark.sql.functions.{col, udf}
import org.apache.spark.sql.types.DateType
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.json4s.jackson.JsonMethods.parse
import org.json4s.{DefaultFormats, FileInput}

/**
  * DataFrame formatting
  */
class Pipe3(conf: Config) extends Pipe(conf) {

  val filejs = new File(conf.getString("file"))

  def run(df: DataFrame, spark: SparkSession):DataFrame = {

    implicit val formats = DefaultFormats
    val jo = parse(FileInput(filejs))
    val sc3 = jo.extract[List[Pipe3ConfRow]]

    val s3cols = sc3.map{case Pipe3ConfRow(existing_col_name, new_col_name, new_data_type, date_expression) =>
      new_data_type match {
        case "date" =>
          val dfFrom = new SimpleDateFormat(date_expression.get)
          val dfTo = new SimpleDateFormat("yyyy-MM-dd")
          val udf1 = udf((date: String) => {
            dfTo.format(dfFrom.parse(date))
          })
          udf1(col(existing_col_name)).cast(DateType).as(new_col_name)
        case _ => col(existing_col_name).cast(new_data_type).as(new_col_name)
      }
    }

    df.select(s3cols:_*)
  }
}

