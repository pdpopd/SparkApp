package ivan.app

import com.typesafe.config.Config
import org.apache.spark.sql.DataFrame
import org.slf4j.LoggerFactory

import scala.collection.JavaConversions._

object Utils {
  private val logger = LoggerFactory.getLogger(getClass)

  def debugPrintDF(df: DataFrame): DataFrame = if (logger.isInfoEnabled) {
    df.printSchema()
    df.show()
    df
  } else df

  def tryo[T](f: => T)
             (implicit onError: Throwable => Option[T] = { t: Throwable => None }): Option[T] = {
    try {
      Some(f)
    } catch {
      case c: Throwable => onError(c)
    }
  }

  def trye[T](f: => T)
             (implicit onError: Throwable => Either[Throwable, T] = { t: Throwable => Left(t) }): Either[Throwable, T] = {
    try {
      Right(f)
    } catch {
      case c: Throwable => onError(c)
    }
  }

  implicit class impConfig(c: Config) {
    def asMap(): Map[String, String] = c.entrySet().toList.map(_.getKey).map(k => k -> c.getString(k)).toMap
  }

}
