package ivan.app

import com.typesafe.config.{Config, ConfigFactory}
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.sql.{DataFrame, Row, SparkSession}
import Utils._
import ivan.app.pipes.Pipe
import collection.JavaConverters._

object Main {

  def  main(args: Array[String]): Unit = {

    val conf = ConfigFactory.load()
    val so = conf.getConfig("spark_options").asMap()
    val pp = conf.getStringList("pipeline")

    //    val warehouseLocation = "file:${system:user.dir}/spark-warehouse"

    if (conf.getBoolean("initSparkContent")) {
      val sparkConf = new SparkConf().setAppName("SparkApp").setMaster("local")
      val sc = new SparkContext(sparkConf)
    }

    val builder = SparkSession
      .builder()
      .appName("SparkApp")

    so.foreach{ case (k, v) => builder.config(k, v)}

    val spark = builder.getOrCreate()
    try {

      pp.asScala.toList.zipWithIndex.map {case (s, i) =>
        Class.forName(s"ivan.app.pipes.$s")
          .getConstructor(classOf[Config])
          .newInstance(conf.getConfig(i.toString))
          .asInstanceOf[Pipe]
      }.foldLeft[DataFrame](null){
        (df, c) => c.run(df, spark)
      }

    } catch {
      case e:Throwable => println("Main "+ e.getMessage)
    } finally {
      spark.close()
    }
  }
}
