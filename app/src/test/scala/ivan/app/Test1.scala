package ivan.app

import java.io.File

import com.typesafe.config.ConfigFactory
import org.specs2.Specification
import org.apache.spark.sql.catalyst.parser.CatalystSqlParser
import org.json4s.{DefaultFormats, FileInput}
import org.json4s.jackson.JsonMethods.parse


class Test1 extends Specification {
  def is = s2"""

  Spark App
    checkConf                                         $checkConf
    parse sql types                                   $e3
    main                                              $e2
    steps                                             e4
                                                      """

  def checkConf = {
    val conf = ConfigFactory.load()

    conf.getBoolean("initSparkContent") === true
    conf.getConfig("9999") must be throwA()
  }

  def e2 = {
//    val (_, t) = Main2.time("main2", {
      Main.main(Array())
//      Main.main(Array())
//    })
//    println(t)
    1===1
  }

  def e3 = {
    Seq("string", "integer", "date", "boolean")
      .map(CatalystSqlParser.parseDataType)
      .map(_.typeName)
      .mkString(",") === "string,integer,date,boolean"
  }

  def e4 = {
//    Main.steps(cli).map(_.getClass.getSimpleName).mkString(",") === "Step1,Step2"

    val jo = parse(FileInput(new File("step3.json")))
    implicit val formats = DefaultFormats

    jo.extract[List[jsRow]].foreach(println)

    1===1
  }
}

case class jsRow(existing_col_name: String, new_col_name: String, new_data_type: String, date_expression: Option[String])