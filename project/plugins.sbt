import sbt.addSbtPlugin
logLevel := Level.Warn

addSbtPlugin("com.typesafe.sbt" % "sbt-native-packager" % "1.3.3")
addSbtPlugin("com.eed3si9n" % "sbt-unidoc" % "0.4.1")